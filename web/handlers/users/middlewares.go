package users

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
)

// UserSessionMiddleware for getting user info from session
func UserSessionMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, err := session.Get("_user_session", c)
		// set user info etc.
		if err != nil || len(sess.Values) <= 0 {
			c.Set("user", nil)
		} else {
			c.Set("user", &UserSession{
				ID:            sess.Values["id"].(uint64),
				Email:         sess.Values["email"].(string),
				NickName:      sess.Values["nick_name"].(string),
				Sex:           sess.Values["sex"].(uint),
				Avatar:        sess.Values["avatar"].(string),
				SigniupTime:   sess.Values["signin_time"].(int64),
				LastLoginTime: sess.Values["last_login_time"].(int64),
				BID:           sess.Values["bid"].(uint64),
				BDomain:       sess.Values["bdomain"].(string),
			})
		}
		next(c)
		return nil
	}
}

// LoginRequire require session to continue(should be use it after UserSessionMiddleware)
func LoginRequire(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Get("user") != nil {
			u := c.Get("user").(*UserSession)
			if u.ID >= 0 && u.Email != "" {
				next(c)
				return nil
			}
		}

		return c.JSON(http.StatusForbidden, &commonResponse{
			Errcode: 13,
		})
	}
}
