package main

import "gitlab.com/echoblog/utils"

var (
	// SERVICENAME microservice name
	SERVICENAME = utils.GetEnvironment("SERVICE_NAME", "go.micro.web.echoblog.api")

	// JAGERAGENTADDR jaeger agent address
	JAGERAGENTADDR = utils.GetEnvironment("JAEGER_AGENT_ADDR", "127.0.0.1:6831")

	// REGISTRYADDR registry address
	REGISTRYADDR = utils.GetEnvironment("REGISTRY_ADDR", "127.0.0.1:8500")

	// WEBADDR web service listen address
	WEBADDR = utils.GetEnvironment("WEB_LISTEN_ADDR", "0.0.0.0:8080")

	// REDISADDR redis address
	REDISADDR = utils.GetEnvironment("REDIS_ADDR", "127.0.0.1:6379")

	// REDISPASS redis password
	REDISPASS = utils.GetEnvironment("REDIS_PASS", "")

	// REDISDATABASE database
	REDISDATABASE = utils.GetEnvironment("REDIS_DATABASE", "echoblog_sessions")
)
