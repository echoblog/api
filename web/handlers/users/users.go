package users

import (
	"net/http"
	"strconv"

	"github.com/gorilla/sessions"

	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	usersproto "gitlab.com/echoblog/users/srv/proto/users"
	"gitlab.com/echoblog/utils"
)

var (
	// users servicename
	servicename = utils.GetEnvironment("SERVICE_USERS_NAME", "go.micro.srv.echoblog.users")
	// login session expire time
	loginexpire, _ = strconv.Atoi(utils.GetEnvironment("LOGIN_EXPIRE_TIME", "24"))
)

// SetRouter set user service router
func SetRouter(g *echo.Group, c *client.Client) {
	h := &usersHandler{
		Srv: usersproto.NewUsersService(servicename, *c),
	}

	g.POST("/", h.reigster)
	g.POST("/session/", h.createsession)

	g.Use(UserSessionMiddleware)
	g.Use(LoginRequire)
	g.PUT("/", h.modifyuser)
	g.GET("/session/", h.getsession)
	g.DELETE("/session/", h.deletesession)
}

// users: session structure

// UserSession userinfo on session
type UserSession struct {
	ID            uint64 `json:"id"`
	Email         string `json:"email"`
	NickName      string `json:"nick_name"`
	Sex           uint   `json:"sex"`
	Avatar        string `json:"avatar"`
	SigniupTime   int64  `json:"signin_time"`
	LastLoginTime int64  `json:"last_login_time"`
	BID           uint64 `json:"-"`
	BDomain       string `json:"-"`
}

// ToMap convert struct to map
func (u *UserSession) ToMap() map[interface{}]interface{} {
	return map[interface{}]interface{}{
		"id":              u.ID,
		"email":           u.Email,
		"nick_name":       u.NickName,
		"sex":             u.Sex,
		"avatar":          u.Avatar,
		"signin_time":     u.SigniupTime,
		"last_login_time": u.LastLoginTime,
		"bid":             u.BID,
		"bdomain":         u.BDomain,
	}
}

// UpdateToSession update user info to session or clear
func (u *UserSession) UpdateToSession(s *sessions.Session, r *http.Request, w http.ResponseWriter, clear bool) error {
	if clear {
		s.Values = nil
	} else {
		s.Values = u.ToMap()
	}
	return s.Save(r, w)
}

// users: requests && response structure
type commonResponse struct {
	Errcode uint   `json:"errcode"`
	Err     string `json:"err"`
}

type reigsterRequest struct {
	Email    string `json:"email,omitempty" validate:"required,email,gte=6,lte=320" maxLength:"320" minLength:"6"`
	Password string `json:"password,omitempty" validate:"required,gte=6,lte=64" maxLength:"64" minLength:"6"`
	Sex      uint   `json:"sex,omitempty" validate:"required,lte=1,gte=0" enums:"0,1" default:"1" required:"false"`
}

type modifyRequest struct {
	OldPass  string `json:"old_pass,omitempty" validate:"omitempty,gte=6,lte=64" maxLength:"64" minLength:"6"`
	Password string `json:"password,omitempty" validate:"omitempty,gte=6,lte=64" maxLength:"64" minLength:"6"`
	Avatar   string `json:"avatar,omitempty" validate:"omitempty,lte=256" maxLength:"256"`
	NickName string `json:"nick_name,omitempty" validate:"omitempty,lte=64,gte=2" maxLength:"64" minLength:"2"`
}

type loginRequest struct {
	Email    string `json:"email,omitempty" validate:"required,email"`
	Password string `json:"password,omitempty" validate:"required"`
}
