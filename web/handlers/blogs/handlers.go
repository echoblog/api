package blogs

import (
	"net/http"
	"strconv"

	"gitlab.com/echoblog/api/web/handlers/users"

	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	opentracing "github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
	blogconfigproto "gitlab.com/echoblog/blogs/srv/proto/blogconfig"
	blogsproto "gitlab.com/echoblog/blogs/srv/proto/blogs"
	categorysproto "gitlab.com/echoblog/blogs/srv/proto/categorys"
	"gitlab.com/echoblog/utils"
)

type blogsHandler struct {
	BlogsSrv      blogsproto.BlogsService
	CategorysSrv  categorysproto.CategorysService
	BlogConfigSrv blogconfigproto.BlogConfigService
}

// @Summary new blog registry
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param	data	body    blogs.blogCreateRequest     true        "blog info"
// @Success 200 object blogs.commonResponse
// @Router /blogs/ [post]
func (h *blogsHandler) blogcreate(c echo.Context) error {
	sp := c.Get(utils.SPAN).(opentracing.Span)
	// define variable && add log
	var req blogCreateRequest
	var res commonResponse
	var err error
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"error":    err.Error(),
		})).Infoln("api.blogs.blogcreate")
		defer sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	user := c.Get("user").(*users.UserSession)
	// micro grpc request
	r, err := h.BlogsSrv.Create(utils.GetContext(&c), &blogsproto.EchoblogBlogsCreateREQ{
		Blog: &blogsproto.Blog{
			Uid:         user.ID,
			Domain:      req.Domain,
			Title:       req.Title,
			Description: req.Description,
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(r.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary get user blog
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param   domain     query    string     false        "blog domain"
// @Success 200 object blogs.blogResponse
// @Router /blogs/ [get]
func (h *blogsHandler) blogget(c echo.Context) error {
	// define variable
	var domain string
	var res blogResponse
	var err error
	var user = &users.UserSession{}
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"requestion_domain": domain,
			"response":          res,
			"error":             err,
			"user":              user,
		})).Infoln("api.blogs.blogget")
		sp.Finish()
	}()
	// get user session data
	if c.Get("user") != nil {
		user = c.Get("user").(*users.UserSession)
	}
	// queryparams
	domain = c.QueryParam("domain")
	// micro grpc request
	if domain != "" {
		r, err := h.BlogsSrv.Get(utils.GetContext(&c), &blogsproto.EchoblogBlogsGetREQ{
			Domain: domain,
		})
		if err != nil {
			res.Errcode = 12
			return c.JSON(http.StatusInternalServerError, res)
		}
		if r.Errcode != 0 || r.Result == nil {
			res.Errcode = uint(r.Errcode)
			return c.JSON(http.StatusOK, res)
		}
		cobj, err := h.BlogConfigSrv.Get(utils.GetContext(&c), &blogconfigproto.EchoblogBlogConfigGetREQ{
			Bid: r.Result.Id,
		})
		if err != nil {
			res.Errcode = 12
			return c.JSON(http.StatusInternalServerError, res)
		}
		if cobj.Errcode != 0 || cobj.Result == nil {
			res.Errcode = uint(cobj.Errcode)
			return c.JSON(http.StatusOK, res)
		}
		if !AllowCheck(cobj.Result, r.Result, user.ID) {
			res.Errcode = 27
		} else {
			res.Result = &Blog{
				Domain:      r.Result.Domain,
				Title:       r.Result.Title,
				Description: r.Result.Description,
				CreatedAt:   r.Result.Createdat,
			}
			res.Errcode = 0
		}
		return c.JSON(http.StatusOK, res)
	}
	r, err := h.BlogsSrv.Get(utils.GetContext(&c), &blogsproto.EchoblogBlogsGetREQ{
		Uid: user.ID,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(r.Errcode)
	if r.Result != nil {
		sess, _ := session.Get("_user_session", c)
		user.BID = r.Result.Id
		user.BDomain = r.Result.Domain
		err = user.UpdateToSession(sess, c.Request(), c.Response(), false)
		res.Result = &Blog{
			Domain:      r.Result.Domain,
			Title:       r.Result.Title,
			Description: r.Result.Description,
			CreatedAt:   r.Result.Createdat,
		}
	}
	return c.JSON(http.StatusOK, res)
}

// @Summary modify blog infomations
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param	data	body    blogs.blogUpdateRequest     true        "blog info"
// @Success 200 object blogs.commonResponse
// @Router /blogs/ [put]
func (h *blogsHandler) blogupdate(c echo.Context) error {
	// define variable
	sp := c.Get(utils.SPAN).(opentracing.Span)
	var req blogUpdateRequest
	var res commonResponse
	var clear bool
	// tracing
	var err error
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"clear":    clear,
			"error":    err.Error(),
		})).Infoln("api.blogs.blogupdate")
		defer sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	user := c.Get("user").(*users.UserSession)
	// micro grpc request
	ur, err := h.BlogsSrv.Update(utils.GetContext(&c), &blogsproto.EchoblogBlogsUpdateREQ{
		Blog: &blogsproto.Blog{
			Id:          user.BID,
			Domain:      user.BDomain,
			Uid:         user.ID,
			Title:       req.Title,
			Description: req.Description,
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(ur.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary get blog configuration
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param   domain     query    string     true        "blog domain"
// @Success 200 object blogs.blogconfigResponse
// @Router /blogs/config/ [get]
func (h *blogsHandler) blogconfigget(c echo.Context) error {
	// define variable
	var domain string
	var res blogconfigResponse
	var err error
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"requestion_domain": domain,
			"response":          res,
			"error":             err,
		})).Infoln("api.blogs.blogconfigget")
		sp.Finish()
	}()
	// queryparams
	domain = c.QueryParam("domain")
	// micro grpc request
	if domain == "" {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	r, err := h.BlogsSrv.Get(utils.GetContext(&c), &blogsproto.EchoblogBlogsGetREQ{
		Domain: domain,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	if r.Errcode != 0 || r.Result == nil {
		res.Errcode = uint(r.Errcode)
		return c.JSON(http.StatusOK, res)
	}
	cobj, err := h.BlogConfigSrv.Get(utils.GetContext(&c), &blogconfigproto.EchoblogBlogConfigGetREQ{
		Bid: r.Result.Id,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	if cobj.Result != nil {
		res.Result = &BlogConfig{
			BlogPermission:    uint(cobj.Result.Blogpermission),
			CommentPermission: uint(cobj.Result.Commentpermission),
		}
	}
	res.Errcode = uint(cobj.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary modify blog configuration
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param	data	body    blogs.blogConfigUpdateRequest     true        "blog info"
// @Success 200 object blogs.commonResponse
// @Router /blogs/config/ [put]
func (h *blogsHandler) blogconfigupdate(c echo.Context) error {
	// define variable
	sp := c.Get(utils.SPAN).(opentracing.Span)
	var req blogConfigUpdateRequest
	var res commonResponse
	var clear bool
	// tracing
	var err error
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"clear":    clear,
			"error":    err.Error(),
		})).Infoln("api.blogs.blogconfigupdate")
		defer sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	user := c.Get("user").(*users.UserSession)
	// micro grpc request
	ur, err := h.BlogConfigSrv.Update(utils.GetContext(&c), &blogconfigproto.EchoblogBlogConfigUpdateREQ{
		Blogconfig: &blogconfigproto.BlogConfig{
			Bid:               user.BID,
			Commentpermission: uint64(req.CommentPermission),
			Blogpermission:    uint64(req.BlogPermission),
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(ur.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary new blog category
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param	data	body    blogs.categoryCreateRequest     true        "category info"
// @Success 200 object blogs.categoryCreateResponse
// @Router /blogs/categorys/ [post]
func (h *blogsHandler) categorycreate(c echo.Context) error {
	sp := c.Get(utils.SPAN).(opentracing.Span)
	// define variable && add log
	var req categoryCreateRequest
	var res categoryCreateResponse
	var err error
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"error":    err.Error(),
		})).Infoln("api.blogs.categorycreate")
		defer sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	user := c.Get("user").(*users.UserSession)
	r, err := h.CategorysSrv.Create(utils.GetContext(&c), &categorysproto.EchoblogCategoryCreateREQ{
		Category: &categorysproto.Category{
			Bid:   user.BID,
			Title: req.Title,
			Index: uint64(req.Index),
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(r.Errcode)
	res.ID = r.Id
	return c.JSON(http.StatusOK, res)
}

// @Summary get blog categorys
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param   domain     query    string     true        "blog domain"
// @Success 200 object blogs.categoryListResponse
// @Router /blogs/categorys/ [get]
func (h *blogsHandler) categorylist(c echo.Context) error {
	// define variable
	var domain string
	var res categoryListResponse
	var user = &users.UserSession{}
	var err error
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"requestion_domain": domain,
			"response":          res,
			"user":              user,
			"error":             err,
		})).Infoln("api.blogs.categorylist")
		sp.Finish()
	}()
	// queryparams
	domain = c.QueryParam("domain")
	if domain == "" {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	if c.Get("user") != nil {
		user = c.Get("user").(*users.UserSession)
	}
	// micro grpc request
	r, err := h.BlogsSrv.Get(utils.GetContext(&c), &blogsproto.EchoblogBlogsGetREQ{
		Domain: domain,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	if r.Errcode != 0 || r.Result == nil {
		res.Errcode = uint(r.Errcode)
		return c.JSON(http.StatusOK, res)
	}
	cobj, err := h.BlogConfigSrv.Get(utils.GetContext(&c), &blogconfigproto.EchoblogBlogConfigGetREQ{
		Bid: r.Result.Id,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	if cobj.Errcode != 0 || cobj.Result == nil {
		res.Errcode = uint(cobj.Errcode)
		return c.JSON(http.StatusOK, res)
	}
	if !AllowCheck(cobj.Result, r.Result, user.ID) {
		res.Errcode = 27
		return c.JSON(http.StatusOK, res)
	}
	lr, err := h.CategorysSrv.List(utils.GetContext(&c), &categorysproto.EchoblogCategoryListREQ{
		Bid: r.Result.Id,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(lr.Errcode)
	for _, item := range lr.Result {
		res.Result = append(res.Result, &Categorys{
			ID:    item.Id,
			Index: uint(item.Index),
			Title: item.Title,
		})
	}
	return c.JSON(http.StatusOK, res)
}

// @Summary get blog category
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param   domain     path    string     true        "blog domain"
// @Param   id     path    int     true        "category id"
// @Success 200 object blogs.categoryGetResponse
// @Router /blogs/categorys/{domain}/{id}/ [get]
func (h *blogsHandler) categoryget(c echo.Context) error {
	// define variable
	var domain string
	var id int
	var res categoryGetResponse
	var user = &users.UserSession{}
	var err error
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"requestion_domain": domain,
			"requestion_id":     id,
			"response":          res,
			"user":              user,
			"error":             err,
		})).Infoln("api.blogs.categoryget")
		sp.Finish()
	}()
	// url params
	domain = c.Param("domain")
	id, err = strconv.Atoi(c.Param("id"))
	if err != nil || domain == "" || id <= 0 {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	if c.Get("user") != nil {
		user = c.Get("user").(*users.UserSession)
	}
	// micro grpc request
	r, err := h.BlogsSrv.Get(utils.GetContext(&c), &blogsproto.EchoblogBlogsGetREQ{
		Domain: domain,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	if r.Errcode != 0 || r.Result == nil {
		res.Errcode = uint(r.Errcode)
		return c.JSON(http.StatusOK, res)
	}
	cobj, err := h.BlogConfigSrv.Get(utils.GetContext(&c), &blogconfigproto.EchoblogBlogConfigGetREQ{
		Bid: r.Result.Id,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	if cobj.Errcode != 0 || cobj.Result == nil {
		res.Errcode = uint(cobj.Errcode)
		return c.JSON(http.StatusOK, res)
	}
	if !AllowCheck(cobj.Result, r.Result, user.ID) {
		res.Errcode = 27
		return c.JSON(http.StatusOK, res)
	}
	gr, err := h.CategorysSrv.Get(utils.GetContext(&c), &categorysproto.EchoblogCategoryGetREQ{
		Id:  uint64(id),
		Bid: r.Result.Id,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(gr.Errcode)
	if gr.Result != nil {
		res.Result = &Categorys{
			ID:    gr.Result.Id,
			Index: uint(gr.Result.Index),
			Title: gr.Result.Title,
		}
	}
	return c.JSON(http.StatusOK, res)
}

// @Summary modify blog category info
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param   id     path    int     true        "category id"
// @Param	data	body    blogs.categoryUpdateRequest     true        "blog info"
// @Success 200 object blogs.commonResponse
// @Router /blogs/categorys/{id}/ [put]
func (h *blogsHandler) categoryupdate(c echo.Context) error {
	// define variable
	var id int
	var req categoryUpdateRequest
	var res commonResponse
	var err error
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":       req,
			"requestion_id": id,
			"response":      res,
			"error":         err.Error(),
		})).Infoln("api.blogs.categoryupdate")
		sp.Finish()
	}()
	// url param
	id, err = strconv.Atoi(c.Param("id"))
	if err != nil || id <= 0 {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	user := c.Get("user").(*users.UserSession)
	// micro grpc request
	r, err := h.CategorysSrv.Update(utils.GetContext(&c), &categorysproto.EchoblogCategoryUpdateREQ{
		Category: &categorysproto.Category{
			Id:    uint64(id),
			Bid:   user.BID,
			Index: uint64(req.Index),
			Title: req.Title,
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(r.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary delete category
// @Tags blogs
// @Accept  json
// @Produce  json
// @Param   id     path    int     true        "category id"
// @Success 200 object blogs.commonResponse
// @Router /blogs/category/{id}/ [delete]
func (h *blogsHandler) categorydelete(c echo.Context) error {
	// define variable
	var id int
	var res commonResponse
	var err error
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"requestion_id": id,
			"response":      res,
			"error":         err.Error(),
		})).Infoln("api.blogs.categorydelete")
		sp.Finish()
	}()
	// url param
	id, err = strconv.Atoi(c.Param("id"))
	if err != nil || id <= 0 {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user session data
	user := c.Get("user").(*users.UserSession)
	// micro grpc request
	r, err := h.CategorysSrv.Delete(utils.GetContext(&c), &categorysproto.EchoblogCategoryDeleteREQ{
		Id:  uint64(id),
		Bid: user.ID,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	res.Errcode = uint(r.Errcode)
	return c.JSON(http.StatusOK, res)
}
