module gitlab.com/echoblog/api/web

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff
	github.com/go-playground/validator v9.24.0+incompatible // indirect
	github.com/gorilla/sessions v1.1.3
	github.com/labstack/echo v3.3.5+incompatible
	github.com/labstack/echo-contrib v0.0.0-20180916154247-4aaa9fadfb82
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/micro/cli v0.0.0-20181223203424-1b0c9793c300 // indirect
	github.com/micro/go-micro v0.14.1
	github.com/micro/go-plugins v0.17.0 // indirect
	github.com/micro/go-web v0.4.0
	github.com/micro/mdns v0.0.0-20181201230301-9c3770d4057a // indirect
	github.com/miekg/dns v1.1.1 // indirect
	github.com/opentracing/opentracing-go v1.0.2
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.0.6
	gitlab.com/echoblog/blogs/srv v0.0.0-20190102164750-43ac8bb0b78c
	gitlab.com/echoblog/users/srv v0.0.0-20190102163929-9c9330f44a76
	gitlab.com/echoblog/utils v0.0.0-20190103121437-ab89878ffe0f
	golang.org/x/crypto v0.0.0-20190102171810-8d7daa0c54b3 // indirect
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3 // indirect
	golang.org/x/sys v0.0.0-20190102155601-82a175fd1598 // indirect
)
