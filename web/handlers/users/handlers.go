package users

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/sessions"

	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	opentracing "github.com/opentracing/opentracing-go"
	usersproto "gitlab.com/echoblog/users/srv/proto/users"
	"gitlab.com/echoblog/utils"
)

type usersHandler struct {
	Srv usersproto.UsersService
}

// @Summary register new user
// @Tags users
// @Accept  json
// @Produce  json
// @Param	user	body    users.reigsterRequest     true        "user info"
// @Success 200 object users.commonResponse
// @Router /users/ [post]
func (u *usersHandler) reigster(c echo.Context) error {
	sp := c.Get(utils.SPAN).(opentracing.Span)
	// define variable && add log
	var req reigsterRequest
	var res commonResponse
	var err error
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"error":    err,
		})).Infoln("api.user.register")
		defer sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// micro grpc request
	r, err := u.Srv.Register(utils.GetContext(&c), &usersproto.EchoblogUsersRegisterREQ{
		User: &usersproto.User{
			Email:    req.Email,
			Password: req.Password,
			Sex:      uint64(req.Sex),
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	// response
	res.Errcode = uint(r.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary modify user profile
// @Tags users
// @Accept  json
// @Produce  json
// @Param	user	body    users.modifyRequest     true        "user info"
// @Success 200 object users.commonResponse
// @Router /users/ [put]
func (u *usersHandler) modifyuser(c echo.Context) error {
	// define variable
	sp := c.Get(utils.SPAN).(opentracing.Span)
	var req modifyRequest
	var res commonResponse
	var clear bool
	// tracing
	var err error
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"clear":    clear,
			"error":    err,
		})).Infoln("api.user.modifyuser")
		defer sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user from session
	sess, _ := session.Get("_user_session", c)
	user := c.Get("user").(*UserSession)
	// micro grpc request
	rpcuser := &usersproto.User{
		Nickname: req.NickName,
		Avatar:   req.Avatar,
		Id:       uint64(user.ID),
		Email:    user.Email,
	}
	// check old password
	if req.Password != "" && req.OldPass != "" {
		ures, err := u.Srv.Get(utils.GetContext(&c), &usersproto.EchoblogUsersGetREQ{
			Id:    uint64(user.ID),
			Email: user.Email,
		})
		if err != nil {
			res.Errcode = 12
			return c.JSON(http.StatusInternalServerError, res)
		}
		if ures.Errcode != 0 {
			res.Errcode = uint(ures.Errcode)
			return c.JSON(http.StatusInternalServerError, res)
		}
		if !utils.BcryptCheck(req.OldPass, ures.Result.Password) {
			res.Errcode = 11
			return c.JSON(http.StatusInternalServerError, res)
		}
		rpcuser.Password = req.Password
		// logout
		sess.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   1,
			Secure:   false,
			HttpOnly: false,
		}
		clear = true
	} else {
		// update session
		user.NickName = req.NickName
		user.Avatar = req.Avatar
	}
	err = user.UpdateToSession(sess, c.Request(), c.Response(), clear)
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}
	r, err := u.Srv.Update(utils.GetContext(&c), &usersproto.EchoblogUsersUpdateREQ{
		User: rpcuser,
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	// response
	res.Errcode = uint(r.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary get current user profile
// @Tags users
// @Accept  json
// @Produce  json
// @Success 200 object users.UserSession
// @Router /users/session/ [get]
func (u *usersHandler) getsession(c echo.Context) error {
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer sp.Finish()
	user := c.Get("user").(*UserSession)
	return c.JSON(http.StatusOK, user)
}

// @Summary login user
// @Tags users
// @Accept  json
// @Produce  json
// @Param	user	body    users.loginRequest     true        "email, password"
// @Success 200 object users.commonResponse
// @Router /users/session/ [post]
func (u *usersHandler) createsession(c echo.Context) error {
	// define variable
	var req loginRequest
	var res commonResponse
	var err error
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer func() {
		log.WithFields(log.Fields(map[string]interface{}{
			"request":  req,
			"response": res,
			"error":    err,
		})).Infoln("api.user.modifyuser")
		sp.Finish()
	}()
	// bind data
	err = c.Bind(&req)
	if err != nil {
		res.Errcode = 1
		return c.JSON(http.StatusBadRequest, res)
	}
	// validate data
	err = c.Validate(&req)
	if err != nil {
		res.Errcode = 1
		res.Err = err.Error()
		return c.JSON(http.StatusBadRequest, res)
	}
	// get user from session
	gres, err := u.Srv.Login(utils.GetContext(&c), &usersproto.EchoblogUsersLogineREQ{
		User: &usersproto.User{
			Email:    req.Email,
			Password: req.Password,
		},
	})
	if err != nil {
		res.Errcode = 12
		return c.JSON(http.StatusInternalServerError, res)
	}
	// set session
	if gres.Errcode == 0 {
		sess, _ := session.Get("_user_session", c)
		us := &UserSession{
			ID:            uint64(gres.User.Id),
			NickName:      gres.User.Nickname,
			Email:         gres.User.Email,
			Avatar:        gres.User.Avatar,
			Sex:           uint(gres.User.Sex),
			SigniupTime:   int64(gres.User.Signuptime),
			LastLoginTime: int64(gres.User.Lastlogintime),
		}
		sess.Values = us.ToMap()
		sess.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   86400 * loginexpire,
			Secure:   false,
			HttpOnly: false,
		}
		if err := sess.Save(c.Request(), c.Response()); err != nil {
			res.Errcode = 14
			res.Err = err.Error()
			return c.JSON(http.StatusInternalServerError, res)
		}
	}
	// response
	res.Errcode = uint(gres.Errcode)
	return c.JSON(http.StatusOK, res)
}

// @Summary logout user
// @Tags users
// @Accept  json
// @Produce  json
// @Success 200 string empty
// @Router /users/session/ [delete]
func (u *usersHandler) deletesession(c echo.Context) error {
	// tracing
	sp := c.Get(utils.SPAN).(opentracing.Span)
	defer sp.Finish()
	sess, _ := session.Get("_user_session", c)
	// clear session
	sess.Values = nil
	sess.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   1,
		Secure:   false,
		HttpOnly: false,
	}
	if err := sess.Save(c.Request(), c.Response()); err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}
	// response
	return c.NoContent(http.StatusOK)
}
