package main

import (
	"net/http"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/labstack/echo/middleware"

	"github.com/boj/redistore"
	"gitlab.com/echoblog/api/web/handlers"
	"gitlab.com/echoblog/utils"

	"github.com/labstack/echo"

	"github.com/micro/go-micro/registry"

	"github.com/micro/go-micro/registry/consul"

	"github.com/micro/go-web"

	opentracing "github.com/opentracing/opentracing-go"
)

// @title EchoBlog API
// @version 1.0
// @description api details for echoblog
// @BasePath /v1
func main() {
	// init logger
	initLogger()

	// init redis
	r, err := redistore.NewRediStore(10, "tcp", REDISADDR, REDISPASS, []byte(REDISDATABASE))
	if err != nil {
		log.WithError(err).Fatalln("connecting to redis error occurred")
	}

	// init tracer
	t, io, err := utils.NewTracer(SERVICENAME, JAGERAGENTADDR)
	if err != nil {
		log.WithError(err).Fatalln("init tracer error occurred")
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)

	// init registry
	consulregistry := consul.NewRegistry(
		registry.Addrs(REGISTRYADDR),
	)

	// init service
	ws := web.NewService(
		web.Name(SERVICENAME),
		web.Registry(consulregistry),
		web.RegisterInterval(time.Second*10),
		web.RegisterTTL(time.Second*15),
		web.Address(WEBADDR),
	)

	// init services client
	c := utils.NewMicroClientForWeb(0, 0, 0, 0, 0, 100, &consulregistry)

	// init echo (set api router && handlers)
	e := echo.New()
	// add log middleware
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: os.Stdout,
	}))
	// ignore panic
	e.Use(middleware.Recover())
	// use gzip
	e.Use(middleware.GzipWithConfig(
		middleware.GzipConfig{
			Level: 5,
		},
	))
	// use validator
	e.Validator = utils.NewValidator()
	handlers.SetRouter(e.Group("/v1"), &c, r)
	// set swagger handler
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet},
	}))
	e.Static("/swagger", "swagger.json")

	// set handler with echo web frameworks
	ws.Handle("/", e)

	// run service
	if err := ws.Run(); err != nil {
		log.WithError(err).Fatalln("starting service error occurred")
	}
}

// set global logger
func initLogger() {
	logFile, err := os.OpenFile(SERVICENAME+".log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		log.SetOutput(os.Stdout)
	}
	log.SetOutput(logFile)
	log.SetFormatter(&log.JSONFormatter{})
}
