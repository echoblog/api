
# Error Codes

```json
[
    {
        0: "successed"
    },
    {
        1: "invalid data"
    },
    {
        2: "users: sign up internal error"
    },
    {
        3: "users: get user internal error"
    },
    },
    {
        4: "users: update user internal error"
    },
    {
        5: "users: insert token internal error"
    },
    {
        6: "users: get token internal error"
    },
    {
        7: "users: invalid token(expired or revoked)"
    },
    {
        8: "users: revoke token internal errors"
    },
    {
        9: "users: user already exists"
    },
    {
        10: "users: user not exists"
    },
    {
        11: "users: incorrect password"
    },
    {
        12: "api: grpc error"
    },
    {
        13: "api: login required"
    },
    {
        14: "api: login failed"
    },
    {
        15: "blogs: get blog config internal error occurred"
    },
    {
        16: "blogs: init blog config internal error occurred"
    },
    {
        17: "blogs: modify blog config internal error occurred"
    },
    {
        18: "blogs: list categorys internal error occurred"
    },
    {
        19: "blogs: get categorys internal error occurred"
    },
    {
        20: "blogs: create categorys internal error occurred"
    },
    {
        21: "blogs: update categorys internal error occurred"
    },
    {
        23: "blogs: get blog internal error occurred"
    },
    {
        24: "blogs: user has already registered blog"
    },
    {
        25: "blogs: create blog internal error occurred"
    },
    {
        26: "blogs: update blog internal error occurred"
    },
    {
        27: "api: you are not allowed to access this blog"
    },
    {
        28: "api: blog does not exists"
    },
]
```