package blogs

import (
	"github.com/labstack/echo"
	"github.com/micro/go-micro/client"
	"gitlab.com/echoblog/api/web/handlers/users"
	blogconfigproto "gitlab.com/echoblog/blogs/srv/proto/blogconfig"
	blogsproto "gitlab.com/echoblog/blogs/srv/proto/blogs"
	categorysproto "gitlab.com/echoblog/blogs/srv/proto/categorys"
	"gitlab.com/echoblog/utils"
)

var (
	// blogs servicename
	servicename = utils.GetEnvironment("SERVICE_BLOGS_NAME", "go.micro.srv.echoblog.blogs")

	// ALLOWEVERYONE permission name
	ALLOWEVERYONE uint64 = 1
	// ALLOWFOLLOWER permission name
	ALLOWFOLLOWER uint64 = 2
)

// SetRouter set user service router
func SetRouter(g *echo.Group, c *client.Client) {
	h := &blogsHandler{
		BlogsSrv:      blogsproto.NewBlogsService(servicename, *c),
		CategorysSrv:  categorysproto.NewCategorysService(servicename, *c),
		BlogConfigSrv: blogconfigproto.NewBlogConfigService(servicename, *c),
	}

	g.GET("/config/", h.blogconfigget)

	g.Use(users.UserSessionMiddleware)
	g.GET("/", h.blogget)

	g.GET("/categorys/", h.categorylist)
	g.GET("/categorys/:domain/:id/", h.categoryget)

	g.Use(users.LoginRequire)
	g.POST("/", h.blogcreate)

	g.Use(BlogRequire)
	g.PUT("/", h.blogupdate)

	g.PUT("/config/", h.blogconfigupdate)

	g.POST("/categorys/", h.categorycreate)
	g.PUT("/categorys/:id/", h.categoryupdate)
	g.DELETE("/categorys/:id/", h.categorydelete)
}

// AllowCheck check blog permission
func AllowCheck(c *blogconfigproto.BlogConfig, b *blogsproto.Blog, uid uint64) bool {
	if c == nil || b == nil {
		return false
	}
	if b.Uid == uid {
		return true
	}
	if c.Blogpermission == ALLOWEVERYONE {
		return true
	}
	return false
}

// request && response structure

// Blog bloginfo
type Blog struct {
	ID          uint64 `json:"-"`
	Domain      string `json:"domain,omitempty"`
	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
	CreatedAt   int64  `json:"created_at,omitempty"`
}

// BlogConfig blog configurations
type BlogConfig struct {
	CommentPermission uint `json:"comment_permission,omitempty"`
	BlogPermission    uint `json:"blog_permission,omitempty"`
}

// Categorys blog categorys
type Categorys struct {
	ID    uint64 `json:"id,omitempty"`
	Index uint   `json:"index,omitempty"`
	Title string `json:"title,omitempty"`
}

type blogResponse struct {
	Result  *Blog  `json:"result"`
	Errcode uint   `json:"errcode"`
	Err     string `json:"err"`
}

type blogconfigResponse struct {
	Result  *BlogConfig `json:"result"`
	Errcode uint        `json:"errcode"`
	Err     string      `json:"err"`
}

type categoryCreateResponse struct {
	ID      uint64 `json:"id,omitempty"`
	Errcode uint   `json:"errcode"`
	Err     string `json:"err"`
}

type categoryListResponse struct {
	Result  []*Categorys `json:"result,omitempty"`
	Errcode uint         `json:"errcode"`
	Err     string       `json:"err"`
}

type categoryGetResponse struct {
	Result  *Categorys `json:"result,omitempty"`
	Errcode uint       `json:"errcode"`
	Err     string     `json:"err"`
}

type commonResponse struct {
	Errcode uint   `json:"errcode"`
	Err     string `json:"err"`
}

type blogCreateRequest struct {
	Domain      string `json:"domain,omitempty" validate:"required,gte=4,lte=16" maxLength:"16" minLength:"4"`
	Title       string `json:"title,omitempty" validate:"required,gte=1,lte=72" maxLength:"72" minLength:"1"`
	Description string `json:"description,omitempty" validate:"lte=500" maxLength:"500" required:"false"`
}

type blogUpdateRequest struct {
	Title       string `json:"title,omitempty" validate:"required,gte=1,lte=72" maxLength:"72" minLength:"1"`
	Description string `json:"description,omitempty" validate:"lte=500" maxLength:"500" required:"false"`
}

type blogConfigUpdateRequest struct {
	CommentPermission uint `json:"comment_permission,omitempty" validate:"required,lte=2,gte=0" enums:"0,1,2" default:"1" required:"true"`
	BlogPermission    uint `json:"blog_permission,omitempty" validate:"required,lte=2,gte=0" enums:"0,1,2" default:"1" required:"true"`
}

type categoryCreateRequest struct {
	Title string `json:"title" validate:"required,lte=32,gte=1" default:"0" required:"true" maxLength:"32" minLength:"1"`
	Index uint   `json:"index" validate:"lte=100,gte=0" default:"0" required:"false" minimum:"0" maximum:"100"`
}

type categoryUpdateRequest struct {
	Title string `json:"title" validate:"lte=32,gte=1" default:"0" required:"true" maxLength:"32" minLength:"1"`
	Index uint   `json:"index" validate:"lte=100,gte=0" default:"0" required:"false" minimum:"0" maximum:"100"`
}
