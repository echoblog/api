package handlers

import (
	"github.com/boj/redistore"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	"github.com/micro/go-micro/client"
	"gitlab.com/echoblog/api/web/handlers/blogs"
	"gitlab.com/echoblog/api/web/handlers/users"
	"gitlab.com/echoblog/utils"
)

// SetRouter set router && handler for echo
func SetRouter(group *echo.Group, microclient *client.Client, redis *redistore.RediStore) {

	// set tracing middleware
	group.Use(utils.TracerMiddleware())
	// set session middleware
	group.Use(session.Middleware(redis))
	// set routers
	users.SetRouter(group.Group("/users"), microclient)
	blogs.SetRouter(group.Group("/blogs"), microclient)
}
