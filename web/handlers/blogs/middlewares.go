package blogs

import (
	"net/http"

	"gitlab.com/echoblog/api/web/handlers/users"

	"github.com/labstack/echo"
)

// BlogRequire require blog session to continue(should be use it after UserSessionMiddleware)
func BlogRequire(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Get("user") != nil {
			u := c.Get("user").(*users.UserSession)
			if u.BID <= 0 || u.BDomain == "" {
				return c.JSON(http.StatusForbidden, &commonResponse{
					Errcode: 28,
				})
			}
		}

		next(c)
		return nil
	}
}
